
public class Pessoa {

	//Atributos da Classe.
	
	private String nome;
	private int idade;
	
	//Metodos de acesso
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
		
	public Pessoa() {
		super();
	}
	//Construtores da classe.
	public Pessoa (String nome,int idade) {
		this.nome = nome;
		this.idade = idade;
	}
	
	//Metodo.
	public String Listar() {
		return "Nome: " +nome + " Idade: " +idade + "\n";
	}
	
	
	

}
