
public class Corrente extends Conta {

	double limite;
	
//	static double init_limite;
	
	public Corrente(int numero, double saldo, double limite) {
		super(numero, saldo);
		this.limite=23000;
		
	}
	
	@Override
	public void sacar(double valor) {		
		if (valor <= getSaldo()) {			
			setSaldo (getSaldo() - valor);
		}else if (valor <= limite) {			
			this.limite = this.limite - valor;
		}		
	}
	@Override
	public void depositar (double valor) {
		if (getSaldo() < 0) {
			this.limite = this.limite + valor;
		}else if (this.limite < 0 ){
			setSaldo(getSaldo() +  valor);
		}
	}
}
