import java.util.Scanner;

public class App {
    
    //Constante
    static final int MAXCONTA = 20;
   
    //vari�vel comum
    static int index = 0;
    
    //Lista de contas
    static Conta[] lista = new Conta[MAXCONTA];
    
    static Scanner tecla = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir conta corrente");
            System.out.println("2-Sacar");
            System.out.println("3-Depositar");
            System.out.println("4-Listar saldo das Contas");
            System.out.println("5-Excluir");
            System.err.println("6-Transferencia.");
            System.out.println("7-Sair.");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirContaCorrente(); break;
                case 2: sacarValor(); break;
                case 3: depositarValor(); break;    
                case 4: listarContas(); break;
                case 5: excluir(); break;
                case 6: break;
            }
        } while (op!=6);       
    }
    public static void excluir(){
    	System.out.println("Digite o numero da conta para excluir.");
    	int num = tecla.nextInt();    
    	for (int i = 0; i < lista.length; i++) {
			if (num == lista[i].getNumero()) {
				lista[i] = null;
			}
			if(lista[i+1] == null) {
				break;
			}
		}
    }
    
    public static void incluirContaCorrente(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o saldo da conta:");
        double saldo = tecla.nextDouble();
        System.out.println("Digite o limite");
        double limite = tecla.nextDouble();        
        //Criar o objeto e inserir na lista
        lista[index++] = new Corrente(num, saldo,limite);
        System.out.println("Conta cadastrada com sucesso!");
    }
    
    public static void sacarValor(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do saque:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].sacar(valor);
                break;
            }
        }
    }
//       public static void transferencia() {
//            int transf, favorecido;
//            double quantia;
//            System.out.println("Defina conta do transferidor:");
//            transf = tecla.nextInt();
//            System.out.println("Defina conta do favorecido:");
//            favorecido = tecla.nextInt();
//            System.out.println("Defina quantia:");
//            quantia = tecla.nextDouble();
//            sacarValor(transf, quantia);
//            depositarValor(favorecido, quantia);
//        }
//    }
    
    public static void depositarValor(){
        //Entrada
        System.out.println("Digite o n�mero da conta:");
        int num = tecla.nextInt();
        System.out.println("Digite o valor do dep�sito:");
        double valor = tecla.nextDouble();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].depositar(valor);
                break;
            }
        }
    }
    
    public static void listarContas(){
        double total = 0;
        System.out.println("N� Conta:........ SALDO:");
        for (int i = 0; i < lista.length-1; i++) {
            if (lista[i] != null){
                System.out.println(lista[i].getNumero()
                                   + "........" +
                                   lista[i].getSaldo());
                //total += lista[i].getSaldo();
                total = total + lista[i].getSaldo();
            }else{
                break;
            }
        }
        System.out.println("Total:........" + total);
    }
    
    
}

